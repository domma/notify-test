
let installEvent = null;

const installButton = document.getElementById('butInstall');
installButton.addEventListener('click', installPWA);

function saveBeforeInstallPromptEvent(evt) {
    installEvent = evt;
    installButton.removeAttribute('hidden');
}

function installPWA(evt) {
    installEvent.prompt();
    evt.srcElement.setAttribute('hidden', true);

}
