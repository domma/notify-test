const CACHE_NAME = "my_cache";

const FILES_TO_CACHE = [
    './offline.html'
];

self.addEventListener('install', (evt) => {
    evt.waitUntil(
        caches.open(CACHE_NAME).then((cache) => {
            console.log("caching offline page");
            return cache.addAll(FILES_TO_CACHE);
        })
    );
});


